package com.devcamp.country.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.country.entities.CCountry;
import com.devcamp.country.repositories.CountryRepository;

@RestController
public class CountryController {
    @Autowired
	private CountryRepository countryRepository;

    @PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newCountry = new CCountry();
            newCountry.setCountryName(cCountry.getCountryName());		
            newCountry.setCountryCode(cCountry.getCountryCode());		
            newCountry.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/countries/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id,@RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry existCountry = countryData.get();
            existCountry.setCountryName(cCountry.getCountryName());
            existCountry.setCountryCode(cCountry.getCountryCode());
            existCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(existCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @GetMapping("/countries/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

    @GetMapping("/countries")
	public List<CCountry> getAllCountry() {
		return countryRepository.findAll();
	}

    @DeleteMapping("/countries/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
            countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
