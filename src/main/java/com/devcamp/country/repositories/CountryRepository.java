package com.devcamp.country.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.country.entities.CCountry;

public interface CountryRepository extends JpaRepository<CCountry, Long> {
    CCountry findByCountryCode(String countryCode);
}
