package com.devcamp.country.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.country.entities.CRegion;

public interface RegionRepostiory extends JpaRepository<CRegion, Long>{
    
}
